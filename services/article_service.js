const Joi = require("joi");
const SQLNoRow = require("../exceptions/sql_no_row");
const NotFoundError = require("../exceptions/not_found_error");
const InternalServerError = require("../exceptions/internal_server_error");
const NotImplemented = require("../exceptions/not_implemented");
const BadRequest = require("../exceptions/bad_request");
const ArticleModel = require("../models/article_model");

class ArticleService {
  constructor(promiseMysqlPool) {
    this.dbPool = promiseMysqlPool;
  }

  async getOneArticle(id) {
    try {
      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.query(
        "SELECT id, title, subtitle, content, createdAt, updatedAt FROM article WHERE id = ?",
        [id]
      );
      if (queryResult[0].length < 1) {
        throw new SQLNoRow();
      }

      connection.release();

      return queryResult[0][0];
    } catch (err) {
      console.error(err.message);

      let error;

      if (err instanceof SQLNoRow) {
        error = new NotFoundError("article is not found");
      } else {
        error = new InternalServerError(
          "an error occurred while getting article"
        );
      }

      throw error;
    }
  }

  async getManyArticle() {
    try {
      const connection = await this.dbPool.getConnection();

      const queryResult = await connection.query(
        "SELECT id, title, subtitle, content, createdAt, updatedAt FROM article ORDER BY createdAt DESC"
      );
      if (queryResult[0].length < 1) {
        throw new NotFoundError("article is not found");
      }

      connection.release();

      return queryResult[0];
    } catch (err) {
      console.error(err.message);

      let error;

      if (err instanceof SQLNoRow) {
        error = new NotFoundError("article is not found");
      } else {
        error = new InternalServerError(
          "an error occurred while getting article"
        );
      }

      throw error;
    }
  }

  async createArticle(params) {
    try {
      // validate the request body of new article.
      await ArticleModel.getCreateArticleModel().validateAsync(params);

      // construct an article entity, it would be the object that used to store to database.
      const article = {
        id: null,
        title: params.title,
        subtitle: params.subtitle,
        content: params.content,
        createdAt: new Date(),
        updatedAt: null,
      };

      // get db connection.
      const connection = await this.dbPool.getConnection();

      // execute query, it will run the command to store article object to db.
      const queryResult = await connection.execute(
        "INSERT INTO article SET title = ?, subtitle = ?, content = ?, createdAt = ?",
        [article.title, article.subtitle, article.content, article.createdAt]
      );

      // release the db connection, it will send the unused connection back to the pool.
      connection.release();

      // override the article id with the new id that returned in query result.
      article.id = queryResult[0].insertId;

      // return the resolved object that could be the demanded data result.
      return article;
    } catch (err) {
      // this block will collect the errors if occurred.

      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }

      throw new InternalServerError("an error occurred while getting article");
    }
  }

  async updateArticle(id, params) {
    try {
      // 1. validate `params`
      await ArticleModel.getCreateArticleModel().validateAsync(params);
      // 2. check article with specific `id`, if exist go to number 4, instead go to number 3
      const connection = await this.dbPool.getConnection();

      let queryResult = await connection.execute(
        "SELECT id, title, subtitle, content, createdAt FROM article WHERE id = ?",
        [id]
      );

      // 3. throw not found error
      if (queryResult[0].length < 1) {
        throw new NotFoundError("article is not found");
      }

      // 4. construct updatedArticle object
      const updatedArticle = {
        id: null,
        title: params.title,
        subtitle: params.subtitle,
        content: params.content,
        createdAt: null,
        updatedAt: new Date(),
      };

      // 5. execute update query to ensure the updatedArticle object was sent to the database
      queryResult = await connection.query(
        "UPDATE article SET title = ?, subtitle = ?, content = ?, updatedAt = ? WHERE id = ?",
        [
          updatedArticle.title,
          updatedArticle.subtitle,
          updatedArticle.content,
          updatedArticle.updatedAt,
          id,
        ]
      );
      if (queryResult[0].length < 1) {
        throw new NotFoundError("article is not found");
      }
      connection.release();

      // 6. return updatedArticle object
      return updatedArticle;
    } catch (err) {
      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }
      throw new InternalServerError("an error occurred while updating article");
    }
  }

  async deleteArticle(id) {
    try {
      const connection = await this.dbPool.getConnection();
      // 1. check article with specific `id`, if exist go to number 3, instead go to number 2
      let queryResult = await connection.execute(
        "SELECT id, title, subtitle, content FROM article WHERE id = ?",
        [id]
      );
      // 2. throw not found error
      if (queryResult[0].length < 1) {
        throw new NotFoundError("article is not found");
      }
      // 3. construct deletedArticle object
      const deletedArticle = {
        id: null,
        title: null,
        subtitle: null,
        content: null,
        createdAt: null,
        updatedAt: null,
      };
      // 4. execute delete query to ensure the data was removed from database's table
      queryResult = await connection.query("DELETE FROM article WHERE id = ?", [
        id,
      ]);
      if (queryResult[0].length < 1) {
        throw new NotFoundError("article is not found");
      }
      connection.release();
      // 5. return deletedArticle object
      return deletedArticle;
    } catch (err) {
      console.error(err.message);

      if (err instanceof Joi.ValidationError) {
        throw new BadRequest(err.message);
      }
      throw new InternalServerError("an error occurred while deleting article");
    }

    //throw new NotImplemented('delete article is not implemented');
  }
}

module.exports = ArticleService;
